console.log('Promises')

function getDataFromSomewhereCallback(cb) {
    // Pravi poziv na neki drugi server ili upit na bazu podataka (nepoznato je kada ce baza ili servere odgovoriti)
    setTimeout(() => {
        cb({
            name: "Deni"
        })
    }, 1000)
}


function getDataFromSomewherePromise() {
    return new Promise((resolve, reject) => {
        // Pravi poziv na neki drugi server ili upit na bazu podataka (nepoznato je kada ce baza ili servere odgovoriti)
        setTimeout(() => {
            if (1 === 1) {
                reject('Imas error')
            }

            resolve({
                name: "Deni"
            })
        }, 1000)
    })
}



getDataFromSomewhereCallback((data) => {
    console.log('getDataFromSomewhereCallback');
    console.log(data); // name: "Deni"
})


getDataFromSomewherePromise()
    .then((data) => {
        console.log('getDataFromSomewherePromise');
        console.log(data);
    })
    .catch((error) => {
        console.log(error);
    })


async function asyncAwaitExample() {
    console.log('getDataFromSomewherePromiseAsync');

    try {
        const data = await getDataFromSomewherePromise();
        console.log(data);
    } catch (error) {
        console.log(error);
    }
}

asyncAwaitExample();