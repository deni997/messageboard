const mySQLDatabase = require('../database/MySQLDatabase');

class CardRepository {

    constructor() {
        this.connection = mySQLDatabase.getConnection();
    }

    getAll(page, limit) {
        return new Promise((resolve, reject) => {
            const offset = page * limit;
            const sqlCountQuery = `SELECT COUNT(*) as count FROM cards`;
            const sqlQuery = `
                SELECT  u.username, 
                        u.id as userId, 
                        u.first_name as firstName, 
                        u.last_name as lastName, 
                        c.id, 
                        c.title,
                        c.description, 
                        c.datetime 
                FROM messageboard.cards as c 
                JOIN messageboard.users as u ON c.user_id=u.id 
                ORDER BY 
                    c.datetime 
                DESC LIMIT 
                    ${offset}, ${limit}`

            this.connection.query(sqlCountQuery, (sqlCountQueryError, sqlCountQueryResults) => {
                if (sqlCountQueryError) return reject(sqlCountQueryError);

                const totalCount = sqlCountQueryResults[0].count;

                this.connection.query(sqlQuery, function (sqlQueryResultsError, sqlQueryResults, fields) {
                    if (sqlQueryResultsError) return reject(sqlQueryResultsError);
                    resolve({ data: sqlQueryResults, totalCount });
                });
            });
        })
    }

    getOne(id) {
        return new Promise((resolve, reject) => {
            const sqlQuery = `SELECT    u.id as userId, 
                                        u.username, 
                                        u.first_name as firstName, 
                                        u.last_name as lastName, 
                                        c.id, 
                                        c.title, 
                                        c.description, 
                                        c.datetime 
                              FROM      cards AS c 
                              JOIN      users as u 
                              ON        c.user_id=u.id 
                              WHERE     c.id='${id}'`
            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) return reject(error);

                resolve(results[0])
            })
        })
    }

    getOneUpdate(id, userId) {
        return new Promise((resolve, reject) => {
            const sqlQuery = `SELECT    c.title,
                                        c.description,
                                        c.datetime, 
                                        c.user_id as userId 
                              FROM      cards as c 
                              WHERE     c.id='${id}' 
                              AND       c.user_id='${userId}'`
            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) return reject(error);

                resolve(results[0])
            })
        })
    }

    insert(card) {

        const insertCardQuery = `
            INSERT INTO     cards (title, description, datetime, user_id)
            VALUES          ("${card.title}", "${card.description}", "${card.datetime}", "${card.userId}");
        `

        return new Promise((resolve, reject) => {
            this.connection.query(insertCardQuery, function (error, results, fields) {
                if (error) return reject(error);

                resolve(results);
            });
        })

    }


    update(card) {

        const updateCardQuery = `
            UPDATE      cards as c
            SET         title = '${card.title}',
                        description = '${card.description}',
                        datetime = '${card.datetime}'
            WHERE       c.id=${card.id}
            AND         c.user_id=${card.userId}
        `

        return new Promise((resolve, reject) => {
            this.connection.query(updateCardQuery, function (error, results, fields) {
                if (error) return reject(error);

                resolve(results);
            });
        })
    }

    delete(userId, cardId) {

        return new Promise((resolve, reject) => {
            const deleteCardQuery = `DELETE FROM    cards as c 
                                     WHERE          c.id=${cardId} 
                                     AND            c.user_id=${userId}`
            this.connection.query(deleteCardQuery, function (error, results, fields) {
                if (error) throw error;

                resolve(results)
            });
        })
    }
}

module.exports = new CardRepository();
