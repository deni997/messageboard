const mySQLDatabase = require('../database/MySQLDatabase');
const moment = require('moment');
class CommentRepository {

    constructor() {
        this.connection = mySQLDatabase.getConnection();
    }

    getAll(cardId, page, limit) {

        return new Promise((resolve, reject) => {
            const offset = page * limit;
            const sqlCountQuery = `SELECT COUNT(*) as count FROM comments as c WHERE c.card_id=${cardId}`
            const sqlQuery = `SELECT    u.id as userId,
                                        u.username, 
                                        u.first_name as firstName, 
                                        u.last_name as lastName, 
                                        c.id, 
                                        c.text, 
                                        c.datetime 
                              FROM      comments as c 
                              JOIN      users as u 
                              ON        c.user_id=u.id 
                              WHERE     c.card_id=${cardId} 
                              ORDER BY  c.datetime 
                              DESC 
                              LIMIT     ${offset}, ${limit}`
            this.connection.query(sqlCountQuery, (sqlCountQueryError, sqlCountQueryResults) => {
                if (sqlCountQueryError) return reject(sqlCountQueryError);
                const totalCount = sqlCountQueryResults[0].count;

                this.connection.query(sqlQuery, function (sqlQueryError, sqlQueryResults, fields) {
                    if (sqlQueryError) return reject(sqlQueryError);

                    resolve({ data: sqlQueryResults, totalCount })
                })
            })
        })
    }

    getOne(id) {


        return new Promise((resolve, reject) => {

            const sqlQuery = `
            SELECT       u.id as userId,
                         u.username,
                         u.first_name as firstName, 
                         u.last_name as lastName, 
                         c.id, 
                         c.text, 
                         c.datetime
            FROM         comments as c
            JOIN         users as u
            ON           c.user_id=u.id
            WHERE        c.id='${id}'`

            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) return reject(error);

                console.log('The solution is: ', results);

                resolve(results[0])

            })
        })
    }

    getOneUpdate(id, userId) {

        const sqlQuery = `SELECT    c.text, 
                                        c.datetime, 
                                        c.user_id as userId 
                              FROM      comments as c 
                              WHERE     c.id='${id}' 
                              AND       c.user_id='${userId}'`

        return new Promise((resolve, reject) => {
            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) return reject(error);

                console.log('The solution is: ', results);

                resolve(results[0])
            })
        })
    }

    insert(comment) {

        const insertCommentQuery = `
            INSERT INTO 
                comments (text, datetime, user_id, card_id)
            VALUES
                ("${comment.text}", "${comment.datetime}", "${comment.userId}", "${comment.cardId}")
        `
        return new Promise((resolve, reject) => {
            this.connection.query(insertCommentQuery, function (error, results, fields) {
                if (error) return reject(error);

                resolve(results);
            })
        })

    }

    update(comment) {

        const updateCommentQuery = `
            UPDATE      comments as c
            SET         c.text = '${comment.text}',
                        c.datetime = '${comment.datetime}'
            WHERE       c.id='${comment.id}'
        `

        return new Promise((resolve, reject) => {
            this.connection.query(updateCommentQuery, function (error, results, fields) {
                if (error) return reject(error);

                resolve(results);
            })
        })

    }

    delete(userId, commId) {

        const deleteCommQuery = `DELETE FROM    comments as c 
                                 WHERE          c.id=${commId} 
                                 AND            c.user_id=${userId}`

        return new Promise((resolve, reject) => {
            this.connection.query(deleteCommQuery, function (error, results, fields) {
                if (error) throw error;

                console.log('The solution is: ', results);
                resolve(results)
            });

        })

    }

}

module.exports = new CommentRepository();
