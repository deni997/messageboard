const mySQLDatabase = require('../database/MySQLDatabase')

class UserRepository {

    constructor() {
        this.connection = mySQLDatabase.getConnection()
    }

    getAll() {

        const sqlQuery = `SELECT    u.id, 
                                    u.first_name as firstName, 
                                    u.last_name as lastName, 
                                    u.username 
                          FROM      messageboard.users as u`

        return new Promise((resolve, reject) => {
            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) return reject(error);

                console.log('The solution is: ', results);
                resolve(results)
            });
        })
    }

    getOne(id) {

        const sqlQuery = `SELECT     u.id, u.first_name as firstName, 
                                     u.last_name as lastName, 
                                     u.username FROM users AS u 
                          WHERE      u.id=${id}`

        return new Promise((resolve, reject) => {
            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) return reject(error);

                console.log('The solution is: ', results);
                if (results && results.length) {
                    resolve(results[0])
                } else {
                    resolve(null)
                }
            })
        })
    }

    getByUsername(username) {
        const getExistingUsername = `SELECT * 
                                     FROM       users as u 
                                     WHERE      u.username = "${username}"`

        return new Promise((resolve, reject) => {
            this.connection.query(getExistingUsername, function (error, results, fields) {
                if (error) return reject(error);

                if (results && results.length) {
                    resolve(results[0])
                } else {
                    resolve(null)
                }
            })
        })
    }

    insert(user) {
        const registerUserQuery = `
            INSERT INTO     users (first_name, last_name, username, password) 
            VALUES          ("${user.firstName}", "${user.lastName}", "${user.username}", "${user.password}");
            `
        return new Promise((resolve, reject) => {
            this.connection.query(registerUserQuery, function (error, results, fields) {
                if (error) return reject(error);

                console.log(results.insertId)
                resolve(results);
            });
        })
    }

    update(user) {
        const updateUserQuery = `
            UPDATE   users as u
             SET     first_name = '${user.firstName}',
                     last_name = '${user.lastName}',
                     username = '${user.username}',
                     password = '${user.password}'
            WHERE    u.id=${user.id}
        `

        return new Promise((resolve, reject) => {
            this.connection.query(updateUserQuery, function (error, results, fields) {
                if (error) return reject(error);

                resolve(results);
            });
        })
    }

    getByUsernameAndPassword(username, password) {

        const sqlQuery = `SELECT    u.id, 
                                    u.first_name as firstName, 
                                    u.last_name as lastName, 
                                    u.username 
                          FROM      users as u 
                          WHERE     u.username = '${username}' 
                          AND       u.password = '${password}'`

        return new Promise((resolve, reject) => {
            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) return reject(error);

                console.log('The solution is: ', results);
                if (results && results.length) {
                    resolve(results[0]);
                } else {
                    resolve(null);
                }
            });
        })
    }

    delete(id) {

        const sqlQuery = `DELETE FROM   users as u 
                          WHERE         u.id=${id}`

        return new Promise((resolve, reject) => {
            this.connection.query(sqlQuery, function (error, results, fields) {
                if (error) throw error;

                console.log('The solution is: ', results);
                resolve(results)
            });
        })

    }
}

module.exports = new UserRepository()