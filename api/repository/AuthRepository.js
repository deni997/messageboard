const mySQLDatabase = require('../database/MySQLDatabase')

class AuthRepository {

    constructor() {
        this.connection = mySQLDatabase.getConnection();
    }

}

module.exports = new AuthRepository();