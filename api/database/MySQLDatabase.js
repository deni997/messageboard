const mysql = require('mysql2');

class MySQLDatabase {
    constructor() {
        let dbConfig = {
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME
        }

        this.connection = mysql.createConnection(dbConfig);
    }

    connect() {
        this.connection.connect()
    }

    getConnection() {
        return this.connection
    }

    endConnection() {
        this.connection.end()
    }
}

module.exports = new MySQLDatabase()