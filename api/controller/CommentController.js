const commentService = require('../service/CommentService');
const auth = require('../utils/auth');

class CommentController {

    constructor(app) {

        app.get('/cards/:id/comments', this.getAllComments.bind(this));

        app.get('/comments/:id', this.getCommentById.bind(this));

        app.post('/comments', auth, this.insertComment.bind(this));

        app.put('/comments/:id', auth, this.updateComment.bind(this));

        app.delete('/comments/:id', auth, this.deleteComment.bind(this));
    }

    async getAllComments(request, response, next) {

        const cardId = request.params.id;

        try {
            const page = parseInt(request.query.page) || 0;
            const limit = parseInt(request.query.limit) || 10;
            const comments = await commentService.getAllComments(cardId, page, limit)
            response.status(200).json(comments);
        } catch (error) {
            next(error)
        }

    }

    async getCommentById(request, response, next) {

        const commentId = request.params.id;

        try {
            const comment = await commentService.getOneComment(commentId);
            response.status(200).json(comment);
        } catch (error) {
            next(error)
        }

    }

    async insertComment(request, response, next) {

        const data = {
            ...request.body,
            userId: `${request.authData.sub}`
        }

        try {
            const insertedComm = await commentService.insertComment(data);
            response.status(201).json(insertedComm);
        } catch (error) {
            next(error)
        }

    }

    async updateComment(request, response, next) {

        const comment = {
            id: request.params.id,
            ...request.body,
            userId: `${request.authData.sub}`
        }

        try {
            const updatedComment = await commentService.updateComment(comment);
            response.status(200).json(updatedComment);
        } catch (error) {
            next(error)
        }

    }

    async deleteComment(request, response, next) {

        const commId = request.params.id;
        const userId = `${request.authData.sub}`

        try {
            const deletedComment = await commentService.deleteComment(userId, commId);
            response.status(200).json(deletedComment);
        } catch (error) {
            next(error)
        }

    }

}

module.exports = CommentController