const userService = require('../service/UserService')
const auth = require('../utils/auth')
const { NotFound } = require('../utils/errors')
const handleError = require('../utils/handleError')
class UserController {

    constructor(app) {
        // Define routes
        app.get('/users', this.getAllUsers.bind(this))

        app.get('/users/:id', this.getUserById.bind(this))

        app.put('/users/update', auth, this.updateUser.bind(this))

        app.delete('/users', auth, this.deleteUser.bind(this))
    }


    async getAllUsers(request, response, next) {
        try {
            const users = await userService.getAllUsers()
            if (!users) {
                throw new NotFound('There are no users in database')
            }
            response.status(200).json(users);
        } catch (error) {
            next(error)
        }
    }

    async getUserById(request, response, next) {
        const userId = request.params.id

        try {
            const user = await userService.getOneUser(userId)
            response.status(200).json(user)
        } catch (error) {
            next(error)
        }
    }

    async updateUser(request, response, next) {
        const user = {
            ...request.body,
            id: `${request.authData.sub}`
        }

        try {
            const updatedUser = await userService.updateUser(user)
            response.status(200).json(updatedUser)
        } catch (error) {
            next(error)
        }
    }

    async deleteUser(request, response, next) {

        const userId = `${request.authData.sub}`

        try {
            const deletedUser = await userService.deleteUser(userId);
            response.status(200).json(deletedUser);
        } catch (error) {
            next(error)
        }

    }
}

module.exports = UserController