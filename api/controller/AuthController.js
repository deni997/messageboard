const authService = require('../service/AuthService');

class AuthController {

    constructor(app) {

        app.post('/login', this.login.bind(this))

        app.post('/register', this.register.bind(this))

    }

    async login(request, response, next) {
        const data = {
            username: request.body.username,
            password: request.body.password
        }

        try {
            const login = await authService.login(data.username, data.password)
            response.status(200).json(login);
        } catch (error) {
            next(error)
        }
    }

    async register(request, response, next) {


        try {
            const insertedUser = await authService.register(request.body)
            response.status(201).json(insertedUser)
        } catch (error) {
            next(error)
        }
    }
}

module.exports = AuthController