const cardService = require('../service/CardService');
const auth = require('../utils/auth');

class CardController {

    constructor(app) {

        app.get('/cards', this.getAllCards.bind(this));

        app.get('/cards/:id', this.getCardById.bind(this));

        app.post('/cards', auth, this.insertCard.bind(this));

        app.put('/cards/:id', auth, this.updateCard.bind(this));

        app.delete('/cards/:id', auth, this.deleteCard.bind(this));
    }

    async getAllCards(request, response, next) {
        try {
            const page = parseInt(request.query.page) || 0;
            const limit = parseInt(request.query.limit) || 10;
            const cards = await cardService.getAllCards(page, limit);
            response.status(200).json(cards);
        } catch (error) {
            next(error)
        }
    }

    async getCardById(request, response, next) {
        const cardId = request.params.id

        try {
            const card = await cardService.getOneCard(cardId);
            response.status(200).json(card);
        } catch (error) {
            next(error)
        }
    }

    async insertCard(request, response, next) {
        try {
            const card = request.body
            card.userId = `${request.authData.sub}`
            const insertedCard = await cardService.insertCard(card)
            response.status(201).json(insertedCard);
        } catch (error) {
            next(error)
        }
    }

    async updateCard(request, response, next) {
        const card = {
            id: request.params.id,
            ...request.body,
        }
        card.userId = `${request.authData.sub}`

        try {
            const updatedCard = await cardService.updateCard(card);
            response.status(200).json(updatedCard);
        } catch (error) {
            next(error)
        }
    }

    async deleteCard(request, response, next) {

        const cardId = request.params.id;
        const userId = `${request.authData.sub}`

        try {
            const deletedCard = await cardService.deleteCard(userId, cardId);
            response.status(200).json(deletedCard);
        } catch (error) {
            next(error)
        }

    }
}

module.exports = CardController
