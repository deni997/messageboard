class GeneralError extends Error {
    constructor(message) {
        super();
        this.message = message;
    }

    getCode() {
        if (this instanceof BadRequest) {
            return {
                status: 400,
                error: 'Bad Request'
            };
        } if (this instanceof Unauthorized) {
            return {
                status: 401,
                error: 'Unauthorized'
            };
        } if (this instanceof Forbidden) {
            return {
                status: 403,
                error: 'Forbidden'
            }
        }
        if (this instanceof NotFound) {
            return {
                status: 404,
                error: 'Not Found'
            }
        }

        if (this instanceof Conflict) {
            return {
                status: 409,
                error: "Conflict"
            }
        }

        return {
            status: 500,
            error: 'Internal Server Error'
        }
    }
}

class BadRequest extends GeneralError { }
class Unauthorized extends GeneralError { }
class NotFound extends GeneralError { }
class Forbidden extends GeneralError { }
class Conflict extends GeneralError { }
module.exports = {
    GeneralError,
    BadRequest,
    Unauthorized,
    NotFound,
    Forbidden,
    Conflict
}