const { GeneralError } = require('../utils/errors')

const handleErrors = (err, req, res, next) => {
    if (err instanceof GeneralError) {
        return res.status(err.getCode().status).json({
            status: err.getCode().status,
            error: err.getCode().error,
            message: err.message,
            path: req.route.path
        })
    }

    return res.status(err.getCode().status).json({
        status: err.getCode().status,
        error: err.getCode().error,
        message: err.message,
        path: req.route.path
    })
}

module.exports = handleErrors;