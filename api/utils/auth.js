const jwt = require('jsonwebtoken');
const { Unauthorized, Forbidden } = require('./errors');
const handle = require('./handleError')
function authenticate(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return handle(new Unauthorized("Unauthorized"), req, res);

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
        if (err) return handle(new Forbidden("Forbidden"), req, res);
        req.authData = data;
        next()
    })
}

module.exports = authenticate;