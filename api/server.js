if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const path = require('path');
const auth = require('./utils/auth')
const cors = require('cors')
const handleErrors = require('./utils/handleError')

app.use(bodyParser.json());


if (process.env.NODE_ENV !== 'production') {
    app.use(cors())
}

const mySqlDatabase = require('./database/MySQLDatabase')
const UserController = require('./controller/UserController');
const CardController = require('./controller/CardController');
const CommentController = require('./controller/CommentController');
const AuthController = require('./controller/AuthController');
const moment = require('moment')

// Init database connection
mySqlDatabase.connect()
// Init controllers
const userController = new UserController(app)
const cardController = new CardController(app);
const commentController = new CommentController(app);
const authController = new AuthController(app);

app.use(handleErrors);

// Server side rendering of static HTML files example
app.get('/home', (req, res) => {
    res.sendFile(path.join(__dirname + '/index.html'));
})

app.get('/about', (req, res) => {
    res.sendFile(path.join(__dirname + '/about.html'));
})

app.get('/hello', auth, (req, res) => {
    res.json({ hello: "Hello" });
})




app.listen(process.env.PORT, () => console.log(`Example app listening at http://localhost:3000`))
