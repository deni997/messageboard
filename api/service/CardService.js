const cardRepository = require('../repository/CardRepository');
const moment = require('moment');
const { NotFound, BadRequest } = require('../utils/errors');

class CardService {

    constructor() {
        console.log('- Init CardService -');
        this.cardRepository = cardRepository;
    }

    getAllCards(page, limit) {

        return new Promise((resolve, reject) => {
            this.cardRepository.getAll(page, limit)
                .then((result) => {
                    const cards = result.data.map((card) => {
                        const user = {
                            id: card.userId,
                            username: card.username,
                            firstName: card.firstName,
                            lastName: card.lastName
                        }

                        card = {
                            id: card.id,
                            title: card.title,
                            description: card.description,
                            datetime: card.datetime,
                            user: user
                        }

                        return card;
                    })

                    if (!cards) {
                        return reject(new NotFound("There are no cards"));
                    }

                    return resolve({
                        data: cards,
                        count: result.totalCount,
                        hasMore: (page + 1) * limit < result.totalCount
                    });
                })
                .catch((error) => {
                    return reject(error);
                })
        })
    }

    getOneCard(id) {

        return new Promise((resolve, reject) => {
            this.cardRepository.getOne(id)
                .then((result) => {
                    const user = {
                        id: result.userId,
                        username: result.username,
                        firstName: result.firstName,
                        lastName: result.lastName
                    }

                    result = {
                        id: result.id,
                        title: result.title,
                        description: result.description,
                        datetime: result.datetime,
                        user: user
                    }

                    if (!result) {
                        return reject(new NotFound("User doesn't exist"))
                    }
                    return resolve(result);
                })
                .catch((error) => {
                    return reject(error);
                })
        })

    }

    getOneCardUpdate(id, userId) {

        return new Promise((resolve, reject) => {
            this.cardRepository.getOneUpdate(id, userId)
                .then((result) => {
                    if (!result) {
                        return reject(new NotFound("Card not found"))
                    }
                    return resolve(result)
                })
                .catch((error) => {
                    return reject(error);
                })
        })

    }

    insertCard(card) {

        return new Promise((resolve, reject) => {
            if (!(card.title && card.title.trim()) ||
                !(card.description && card.description.trim()) ||
                !(card.userId && card.userId.trim())) {
                return reject(new BadRequest("Wrong input data"));

            }
            card.datetime = moment().utc().format("YYYY-MM-DD HH:mm:ssZ")

            this.cardRepository.insert(card)
                .then(async (result) => {
                    const oneCard = await this.getOneCard(result.insertId)
                    return resolve(oneCard)
                })
                .catch((error) => {
                    return reject(error)
                })

        })

    }


    updateCard(card) {

        return new Promise((resolve, reject) => {
            this.cardRepository.getOneUpdate(card.id, card.userId)
                .then((existingCard) => {
                    if (!existingCard) {
                        return reject(new NotFound("Card not found"));
                    }

                    // Ulazni datum se salje bez Z (zero hour offset" also known as "Zulu time")
                    // Datum koji se zapisuje u bazu je 0 offset
                    // Datum koji baza vraca je nulto vrijeme sa Z oznakom na kraju (sto ce browser procitati u lokalno vrijeme)
                    const cardDataUpdate = {
                        id: card.id,
                        title: (card.title && card.title.trim()) ? card.title : existingCard.title,
                        description: (card.description && card.description.trim()) ? card.description : existingCard.description,
                        datetime: moment(existingCard.datetime).format("YYYY-MM-DD HH:mm:ss"),
                        userId: existingCard.userId
                    }

                    this.cardRepository.update(cardDataUpdate)
                        .then((results) => {
                            resolve(cardDataUpdate)
                        })
                        .catch((error) => {
                            reject(error);
                        })
                })
        })

    }

    deleteCard(userId, cardId) {

        return new Promise((resolve, reject) => {
            this.cardRepository.delete(userId, cardId)
                .then((result) => {
                    if (result && result.affectedRows) {
                        resolve("Card successfully deleted");
                    } else {
                        reject(new NotFound("Not found"));
                    }
                })
                .catch((error) => {
                    reject(error);
                })
        })
    }
}

module.exports = new CardService();
