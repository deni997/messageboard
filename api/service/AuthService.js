const authRepository = require('../repository/AuthRepository')
const userRepository = require('../repository/UserRepository')
const jwt = require('jsonwebtoken');
const { GeneralError, BadRequest, NotFound, Conflict, Forbidden } = require('../utils/errors')

class AuthService {
    constructor() {
        console.log('- Init AuthService -');
        this.authRepository = authRepository;
        this.userRepository = userRepository;
    }

    login(username, password) {
        return new Promise((resolve, reject) => {
            this.userRepository.getByUsernameAndPassword(username, password)
                .then((result) => {
                    if (result != null) {
                        const payload = {
                            sub: result.id,
                            username: result.username
                        }

                        const { id, username, firstName, lastName } = result;

                        const user = {
                            id,
                            username,
                            firstName,
                            lastName
                        }
                        const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '2h' })

                        return resolve({ user: user, token: accessToken });
                    } else {
                        return reject(new Forbidden("Incorrect username or password"));
                    }
                })
                .catch((err) => {
                    return reject(err);
                })
        })
    }

    register(user) {
        return new Promise((resolve, reject) => {
            if (!(user.firstName && user.firstName.trim()) ||
                !(user.lastName && user.lastName.trim()) ||
                !(user.username && user.username.trim()) ||
                !(user.password && user.password.trim())) {
                return reject(new BadRequest("Wrong input data"))
            }

            this.userRepository.getByUsername(user.username)
                .then((existingUser) => {
                    if (existingUser !== null) {
                        return reject(new Conflict("Username already exists"))
                    }

                    this.userRepository.insert(user)
                        .then((result) => {
                            user = {
                                id: result.insertId,
                                username: user.username,
                                firstName: user.firstName,
                                lastName: user.lastName
                            }
                            return resolve(user)
                        })
                        .catch((error) => {
                            return reject(error)
                        })
                })
                .catch((error) => {
                    return reject(error)
                })
        })
    }
}

module.exports = new AuthService()