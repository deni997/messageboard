const commentRepository = require('../repository/CommentRepository');
const moment = require('moment');
const { GeneralError, NotFound, BadRequest } = require('../utils/errors')
class CommentService {

    constructor() {
        console.log('- Init CommentService -');
        this.commentRepository = commentRepository;
    }

    getAllComments(cardId, page, limit) {

        return new Promise((resolve, reject) => {

            this.commentRepository.getAll(cardId, page, limit)
                .then((result) => {

                    const comments = result.data.map((comment) => {

                        const user = {
                            id: comment.userId,
                            username: comment.username,
                            firstName: comment.firstName,
                            lastName: comment.lastName
                        }

                        comment = {
                            id: comment.id,
                            text: comment.text,
                            datetime: comment.datetime,
                            user: user
                        }

                        return comment;
                    })

                    if (!comments) {
                        return reject(new NotFound("Not found"));
                    }

                    return resolve({
                        data: comments,
                        count: result.totalCount,
                        hasMore: (page + 1) * limit < result.totalCount
                    });
                })
                .catch((error) => {
                    return reject(error);
                })
        })

    }

    getOneComment(id, userId) {

        return new Promise((resolve, reject) => {

            this.commentRepository.getOne(id, userId)
                .then((result) => {

                    const user = {
                        id: result.userId,
                        username: result.username,
                        firstName: result.firstName,
                        lastName: result.lastName
                    }

                    result = {
                        id: result.id,
                        text: result.text,
                        datetime: result.datetime,
                        user: user
                    }

                    if (!result) {
                        return reject(new NotFound("Not found"));
                    }
                    return resolve(result);
                })
                .catch((error) => {
                    return reject(error);
                })

        })

    }

    getOneCommentUpdate(id, userId) {

        return new Promise((resolve, reject) => {
            this.cardRepository.getOneUpdate(id, userId)
                .then((result) => {
                    if (!result) {
                        return reject(new NotFound("Not found"))
                    }
                    return resolve(result)
                })
                .catch((error) => {
                    return reject(error);
                })
        })

    }

    insertComment(comment) {

        return new Promise((resolve, reject) => {
            if (!(comment.text && comment.text.trim()) ||
                !(comment.userId && comment.userId.trim()) ||
                !(comment.cardId && comment.cardId)) {
                return reject(new BadRequest("Wrong input data"));
            }
            comment.datetime = moment().utc().format("YYYY-MM-DD HH:mm:ssZ")

            this.commentRepository.insert(comment)
                .then(async (result) => {
                    const insertComment = await this.getOneComment(result.insertId);
                    return resolve(insertComment);
                })
                .catch((error) => {
                    return reject(error);
                })
        })

    }

    updateComment(comment) {

        return new Promise((resolve, reject) => {
            this.commentRepository.getOneUpdate(comment.id, comment.userId)
                .then((existingComm) => {
                    if (!existingComm) {
                        return reject(new NotFound("Comment not found"));
                    }

                    const commentDataUpdate = {
                        id: comment.id,
                        text: (comment.text && comment.text.trim()) ? comment.text : existingComm.text,
                        datetime: moment(existingComm.datetime).format("YYYY-MM-DD HH:mm:ss"),
                        userId: existingComm.userId
                    }

                    this.commentRepository.update(commentDataUpdate)
                        .then((results) => {
                            return resolve(commentDataUpdate)
                        })
                        .catch((error) => {
                            return reject(error);
                        })
                })
        })

    }

    deleteComment(userId, commId) {
        return new Promise((resolve, reject) => {

            this.commentRepository.delete(userId, commId)
                .then((result) => {
                    if (result && result.affectedRows) {
                        return resolve("Comment successfully deleted");
                    } else {
                        return reject(new NotFound("Not found"));
                    }
                })
                .catch((error) => {
                    return reject(error);
                })

        })
    }

}

module.exports = new CommentService();