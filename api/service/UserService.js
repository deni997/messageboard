const userRepository = require('../repository/UserRepository')
const { GeneralError, NotFound, BadRequest } = require('../utils/errors')
class UserService {
    constructor() {
        console.log('- Init UserService -')
        this.userRepository = userRepository
    }

    getAllUsers() {
        return new Promise((resolve, reject) => {
            this.userRepository.getAll()
                .then((result) => {
                    return resolve(result)
                })
                .catch((error) => {
                    return reject(error)
                })
        })
    }

    getOneUser(id) {
        return new Promise((resolve, reject) => {
            this.userRepository.getOne(id)
                .then((result) => {
                    if (!result) {
                        return reject(new NotFound("User doesn't exist"))
                    }
                    return resolve(result);
                })
                .catch((error) => {
                    return reject(error)
                })
        })
    }

    updateUser(user) {
        return new Promise((resolve, reject) => {

            this.userRepository.getOne(user.id)
                .then((existingUser) => {
                    if (!existingUser) {
                        return reject(new GeneralError("Database error"))
                    }

                    const userDataUpdate = {
                        id: existingUser.id,
                        firstName: (user.firstName && user.firstName.trim()) ? user.firstName : existingUser.firstName,
                        lastName: (user.lastName && user.lastName.trim()) ? user.lastName : existingUser.lastName,
                        username: (user.username && user.username.trim()) ? user.username : existingUser.username,
                        password: (user.password && user.password.trim()) ? user.password : existingUser.password
                    }

                    this.userRepository.update(userDataUpdate)
                        .then((results) => {
                            return resolve(userDataUpdate)
                        })
                        .catch((error) => {
                            return reject(error)
                        })
                })
        })
    }

    deleteUser(id) {
        return new Promise((resolve, reject) => {
            this.userRepository.delete(id)
                .then((result) => {
                    if (result && result.affectedRows) {
                        return resolve(id);
                    } else {
                        return reject(new NotFound("Not found"));
                    }
                })
                .catch((error) => {
                    return reject(error);
                })
        })
    }
}

module.exports = new UserService()