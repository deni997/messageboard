import ApiService from './api.service';
import AppError from './errors/errors';

class CardsService {
  async getCards(params) {
    const url = '/cards';

    try {
      const response = await ApiService.get(url, params);
      return response.data;
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }

  async getCard(id) {
    const url = `/cards/${id}`;

    try {
      const response = await ApiService.get(url);
      return response.data;
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }

  async getCardComments(id, params) {
    const url = `/cards/${id}/comments`;

    try {
      const response = await ApiService.get(url, params);
      return response.data;
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }

  async insertCardComment(comment) {
    const url = '/comments';

    try {
      const response = await ApiService.post(url, comment);
      return response.data;
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }

  async createCard(card) {
    const url = '/cards';

    try {
      const response = await ApiService.post(url, card);
      return response;
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }

  async deleteCard(id) {
    const url = `/cards/${id}`;

    try {
      const response = await ApiService.delete(url);
      return response;
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }
}

export default new CardsService();
