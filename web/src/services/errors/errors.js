export default class AppError extends Error {
  constructor(errorCode, data) {
    super(data);
    this.name = this.constructor.name;
    this.message = data.message;
    this.type = data.error;
    this.errorCode = errorCode;
  }
}
