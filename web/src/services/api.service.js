import TokenService from './token.service';

class ApiService {
  init(axios, baseURL) {
    this.axios = axios;
    this.axios.defaults.baseURL = baseURL;
  }

  setHeader() {
    this.axios.defaults.headers.common.Authorization = `Bearer ${TokenService.getToken()}`;
  }

  removeHeader() {
    this.axios.defaults.headers.common = {};
  }

  get(resource, params) {
    return this.axios.get(resource, { params });
  }

  post(resource, data) {
    return this.axios.post(resource, data);
  }

  put(resource, data) {
    return this.axios.put(resource, data);
  }

  delete(resource) {
    return this.axios.delete(resource);
  }

  customRequest(data) {
    return this.axios(data);
  }
}

export default new ApiService();
