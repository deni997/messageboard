import store from '../../store';

export default (axios) => {
  axios.interceptors.response.use(
    (response) => response,
    async (error) => {
      console.log(error);
      if (error.request.status === 401) {
        if (error.config.url.includes('/login')) {
          // Refresh token has failed. Logout the user
          store.dispatch('auth/logout');
          throw error;
        }
        // else {
        //   // Refresh the access token
        //   try {
        //     await store.dispatch('auth/refreshToken');
        //     // Retry the original request
        //     return this.customRequest({
        //       method: error.config.method,
        //       url: error.config.url,
        //       data: error.config.data,
        //     });
        //   } catch (e) {
        //     // Refresh has failed - reject the original request
        //     throw error;
        //   }
        // }
      }

      // If error was not 401 just reject as is
      throw error;
    },
  );
};
