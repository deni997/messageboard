import ApiService from './api.service';
import TokenService from './token.service';
import AppError from './errors/errors';

class AuthService {
  async login(username, password) {
    const requestData = {
      method: 'post',
      url: '/login',
      data: {
        username,
        password,
      },
    };

    try {
      const response = await ApiService.customRequest(requestData);
      TokenService.saveToken(response.data.token);
      TokenService.saveAuthUser(response.data.user);
      ApiService.setHeader();

      // TokenService.saveRefreshToken(response.data.refresh_token);
      // ApiService.mount401Interceptor();

      return response.data;
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }

  logout() {
    // Remove the token and remove Authorization header from Api Service as well
    TokenService.removeToken();
    TokenService.removeAuthUser();
    ApiService.removeHeader();

    // TokenService.removeRefreshToken();
    // ApiService.unmount401Interceptor();
  }

  async register(user) {
    try {
      return await ApiService.post('/register', user);
    } catch (error) {
      throw new AppError(error.response.status, error.response.data);
    }
  }
}

export default new AuthService();
