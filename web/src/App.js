import Navbar from './components/Navbar/Navbar.vue';

export default {
  name: 'App',
  components: {
    Navbar,
  },
};
