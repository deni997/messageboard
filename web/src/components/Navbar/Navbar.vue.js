import { mapGetters } from 'vuex';

export default {
  name: 'Navbar',
  computed: {
    ...mapGetters('auth', [
      'loggedIn',
      'authenticatedUser',
    ]),
  },
};
