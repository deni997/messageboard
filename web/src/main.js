import Vue from 'vue';
import Notifications from 'vue-notification';
import axios from 'axios';
import moment from 'moment';
import App from './App.vue';
import router from './router';
import store from './store';

import registerAuthInterceptor from './services/interceptors/auth.interceptor';
import ApiService from './services/api.service';
import TokenService from './services/token.service';

Vue.config.productionTip = false;

registerAuthInterceptor(axios);
// Set the base URL of the API
ApiService.init(axios, 'http://localhost:3000');

// If token exists set header
if (TokenService.getToken()) {
  ApiService.setHeader();
}

Vue.filter('formatDate', (value) => {
  if (value) {
    return moment(String(value))
      .format('MM/DD/YYYY @ hh:mm');
  }
});

Vue.use(Notifications);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
