import { mapActions, mapGetters, mapState } from 'vuex';

export default {
  name: 'Home',
  data() {
    return {
      deleteInProgress: false,
    };
  },
  computed: {
    ...mapState('cards', [
      'cards',
      'cardsLoading',
      'cardsError',
      'cardsParams',
    ]),
    ...mapGetters('auth', [
      'authenticatedUser',
    ]),
  },
  methods: {
    ...mapActions('cards', [
      'getCardsAction',
      'deleteCardAction',
      'clearCardsAction',
    ]),
    async loadMore() {
      this.cardsParams.page += 1;
      await this.getCardsAction(this.cardsParams);
    },
    async deleteCard(id) {
      this.deleteInProgress = true;
      await this.deleteCardAction(id);
      this.deleteInProgress = false;
    },
    goToCard(id) {
      this.$router.push({
        path: `/cards/${id}`,
      });
    },
  },
  async created() {
    await this.clearCardsAction();
    await this.getCardsAction(this.cardsParams);
  },
};
