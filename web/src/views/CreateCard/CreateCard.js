import { mapActions, mapState } from 'vuex';

export default {
  name: 'CreateCard',
  data() {
    return {
      title: '',
      description: '',
    };
  },
  computed: {
    ...mapState('cards', [
      'createCardLoading',
    ]),
  },
  methods: {
    ...mapActions('cards', [
      'createCardAction',
    ]),
    async createCard() {
      if (this.createCardLoading) {
        return;
      }

      await this.createCardAction({
        title: this.title,
        description: this.description,
      });
    },
  },
};
