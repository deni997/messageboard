import { mapActions } from 'vuex';

export default {
  name: 'Register',
  data() {
    return {
      firstname: '',
      lastname: '',
      username: '',
      password: '',
    };
  },
  methods: {
    ...mapActions('auth', [
      'registerAction',
    ]),
    register() {
      this.registerAction({
        firstName: this.firstname,
        lastName: this.lastname,
        username: this.username,
        password: this.password,
      });
    },
  },
};
