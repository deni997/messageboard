import { mapActions, mapGetters } from 'vuex';

export default {
  name: 'Login',
  data() {
    return {
      username: '',
      password: '',
    };
  },
  computed: {
    ...mapGetters('auth', [
      'authenticating',
      'authenticationError',
      'authenticationErrorCode',
    ]),
  },
  methods: {
    ...mapActions('auth', [
      'loginAction',
      'logoutAction',
    ]),

    async handleSubmit() {
      console.log(this.username, this.password);
      // Perform a simple validation that email and password have been typed in
      if (this.username !== '' && this.password !== '') {
        await this.loginAction({
          username: this.username,
          password: this.password,
        });
      }
    },
  },
  mounted() {
    this.logoutAction();
  },
};
