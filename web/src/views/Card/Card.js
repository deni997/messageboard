import { mapActions, mapGetters, mapState } from 'vuex';

export default {
  name: 'Card',
  data() {
    return {
      comment: '',
    };
  },
  computed: {
    ...mapState('cards', [
      'card',
      'cardLoading',
      'cardError',
      'cardComments',
      'cardCommentsLoading',
      'cardCommentsError',
      'cardCommentsParams',
    ]),
    ...mapGetters('auth', [
      'loggedIn',
    ]),
  },
  methods: {
    ...mapActions('cards', [
      'getCardAction',
      'getCardCommentsAction',
      'createCardCommentsAction',
      'clearCardCommentsAction',
    ]),
    async loadCard() {
      const { id } = this.$route.params;
      await this.getCardAction(id);
      await this.getCardCommentsAction({ id, params: this.cardCommentsParams });
    },
    async loadMore() {
      const { id } = this.$route.params;
      this.cardCommentsParams.page += 1;
      await this.getCardCommentsAction({ id, params: this.cardCommentsParams });
    },
    async applyComment() {
      const { id } = this.$route.params;
      const comment = {
        text: this.comment,
        cardId: id,
      };

      await this.createCardCommentsAction(comment);
      this.comment = '';
    },
  },
  async created() {
    await this.clearCardCommentsAction();
    await this.loadCard();
  },
};
