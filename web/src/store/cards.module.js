import Vue from 'vue';
import * as types from './mutation-types';
import CardsService from '../services/cards.service';
import router from '../router';

function initialState() {
  return {
    // Home
    cards: {
      data: [],
      hasMore: false,
    },
    cardsParams: {
      page: 0,
      limit: 5,
    },
    cardsLoading: false,
    cardsError: undefined,
    // Card
    card: undefined,
    cardLoading: undefined,
    cardError: undefined,
    cardComments: {
      data: [],
      hasMore: false,
    },
    cardCommentsParams: {
      page: 0,
      limit: 10,
    },
    cardCommentsLoading: false,
    cardCommentsError: undefined,
    // Create Card
    createCardLoading: false,
  };
}

const state = initialState();

const getters = {};

const actions = {
  async getCardsAction({ commit }, params) {
    commit(types.GET_CARDS);

    try {
      const cards = await CardsService.getCards(params);
      const payload = {
        cards,
        params,
      };

      commit(types.GET_CARDS_SUCCESS, payload);
    } catch (error) {
      commit(types.GET_CARDS_ERROR, error);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });
    }
  },
  async clearCardsAction({ commit }) {
    commit(types.CLEAR_CARDS);
  },
  async createCardAction({ commit }, card) {
    commit(types.CREATE_CARD);

    try {
      const createdCard = await CardsService.createCard(card);
      commit(types.CREATE_CARD_SUCCESS, createdCard);
      await router.push('/');
    } catch (error) {
      commit(types.CREATE_CARD_ERROR, error);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });
    }
  },
  async deleteCardAction({ commit }, id) {
    try {
      await CardsService.deleteCard(id);

      commit(types.DELETE_CARD_SUCCESS, id);

      Vue.notify({
        group: 'success',
        title: `Card ${id} deleted`,
        text: 'Card is successfully deleted',
      });
    } catch (error) {
      commit(types.DELETE_CARD_ERROR, error);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });
    }
  },
  async getCardAction({ commit }, id) {
    commit(types.GET_CARD);

    try {
      const card = await CardsService.getCard(id);
      commit(types.GET_CARD_SUCCESS, card);
    } catch (error) {
      commit(types.GET_CARD_ERROR, error);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });
    }
  },
  async getCardCommentsAction({ commit }, { id, params }) {
    commit(types.GET_CARD_COMMENTS);

    try {
      const comments = await CardsService.getCardComments(id, params);
      commit(types.GET_CARD_COMMENTS_SUCCESS, {
        comments,
        params,
      });
    } catch (error) {
      commit(types.GET_CARD_COMMENTS_ERROR, error);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });
    }
  },
  async createCardCommentsAction({ commit }, comment) {
    commit(types.CREATE_CARD_COMMENT);

    try {
      const insertedComment = await CardsService.insertCardComment(comment);

      commit(types.CREATE_CARD_COMMENT_SUCCESS, insertedComment);
    } catch (error) {
      commit(types.CREATE_CARD_COMMENT_ERROR, error);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });
    }
  },
  async clearCardCommentsAction({ commit }) {
    commit(types.CLEAR_CARD_COMMENTS);
  },
};

const mutations = {
  [types.GET_CARDS](state) {
    state.cardsLoading = true;
    state.cardsError = undefined;
  },

  [types.GET_CARDS_SUCCESS](state, { cards, params }) {
    state.cardsLoading = false;
    state.cards.data.push(...cards.data);
    state.cards.hasMore = cards.hasMore;
    state.cardsError = undefined;
    state.cardsParams = params;
  },

  [types.GET_CARDS_ERROR](state, error) {
    state.cardsLoading = false;
    state.cards = initialState().cards;
    state.cardsError = error;
  },

  [types.CLEAR_CARDS](state) {
    state.cards = initialState().cards;
    state.cardsParams = initialState().cardsParams;
    state.cardsLoading = false;
    state.cardsError = undefined;
  },

  [types.GET_CARD](state) {
    state.card = undefined;
    state.cardLoading = true;
    state.cardError = undefined;
  },

  [types.GET_CARD_SUCCESS](state, card) {
    state.card = card;
    state.cardLoading = false;
    state.cardError = undefined;
  },

  [types.GET_CARD_ERROR](state, error) {
    state.card = undefined;
    state.cardLoading = false;
    state.cardError = error;
  },

  [types.CREATE_CARD](state) {
    state.createCardLoading = true;
  },

  [types.CREATE_CARD_SUCCESS](state) {
    state.createCardLoading = false;
  },

  [types.CREATE_CARD_ERROR](state) {
    state.createCardLoading = false;
  },

  [types.DELETE_CARD](state) {
    state.cardsLoading = true;
    state.cardsError = undefined;
  },

  [types.DELETE_CARD_SUCCESS](state, id) {
    state.cardsLoading = false;
    state.cards.data = state.cards.data.filter((card) => card.id !== id);
    state.cards.count -= 1;
  },

  [types.DELETE_CARD_ERROR](state, error) {
    state.cardsLoading = false;
    state.cardsError = error;
  },

  [types.GET_CARD_COMMENTS](state) {
    state.cardCommentsLoading = true;
    state.cardCommentsError = undefined;
  },

  [types.GET_CARD_COMMENTS_SUCCESS](state, { comments, params }) {
    state.cardComments.data.push(...comments.data);
    state.cardComments.hasMore = comments.hasMore;
    state.cardCommentsParams = params;
    state.cardCommentsLoading = false;
    state.cardCommentsError = undefined;
  },

  [types.GET_CARD_COMMENTS_ERROR](state, error) {
    state.cardComments = initialState().cardComments;
    state.cardCommentsLoading = false;
    state.cardCommentsError = error;
  },

  [types.CLEAR_CARD_COMMENTS](state) {
    state.cardComments = initialState().cardComments;
    state.cardCommentsParams = initialState().cardCommentsParams;
    state.cardCommentsLoading = false;
    state.cardCommentsError = undefined;
  },

  [types.CREATE_CARD_COMMENT](state) {
    state.cardCommentsLoading = true;
    state.cardCommentsError = undefined;
  },

  [types.CREATE_CARD_COMMENT_SUCCESS](state, comment) {
    state.cardComments.data.unshift(comment);
    state.cardCommentsLoading = false;
    state.cardCommentsError = undefined;
  },

  [types.CREATE_CARD_COMMENT_ERROR](state, error) {
    state.cardCommentsLoading = false;
    state.cardCommentsError = error;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
