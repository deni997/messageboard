export const GET_CARDS = 'getCards';
export const GET_CARDS_SUCCESS = 'getCardsSuccess';
export const GET_CARDS_ERROR = 'getCardsError';
export const CLEAR_CARDS = 'clearCards';

export const GET_CARD = 'getCard';
export const GET_CARD_SUCCESS = 'getCardSuccess';
export const GET_CARD_ERROR = 'getCardError';

export const DELETE_CARD = 'deleteCard';
export const DELETE_CARD_SUCCESS = 'deleteCardSuccess';
export const DELETE_CARD_ERROR = 'deleteCardError';

export const CREATE_CARD = 'createCard';
export const CREATE_CARD_SUCCESS = 'createCardSuccess';
export const CREATE_CARD_ERROR = 'createCardError';

export const GET_CARD_COMMENTS = 'getCardComments';
export const GET_CARD_COMMENTS_SUCCESS = 'getCardCommentsSuccess';
export const GET_CARD_COMMENTS_ERROR = 'getCardCommentsError';

export const CREATE_CARD_COMMENT = 'createCardComment';
export const CREATE_CARD_COMMENT_SUCCESS = 'createCardCommentSuccess';
export const CREATE_CARD_COMMENT_ERROR = 'createCardCommentError';
export const CLEAR_CARD_COMMENTS = 'clearCardComments';

export const REGISTER = 'register';
export const REGISTER_SUCCESS = 'registerSuccess';
export const REGISTER_ERROR = 'registerError';

export const LOGIN = 'login';
export const LOGIN_SUCCESS = 'loginSuccess';
export const LOGIN_ERROR = 'loginError';

export const LOGOUT_SUCCESS = 'logoutSuccess';
