import Vue from 'vue';
import * as types from './mutation-types';
import AuthService from '../services/auth.service';
import TokenService from '../services/token.service';
import router from '../router';

const state = {
  authenticatedUser: TokenService.getAuthUser(),
  authenticating: false,
  accessToken: TokenService.getToken(),
  // Register
  registerLoading: false,
  registerError: undefined,
};

const getters = {
  authenticatedUser: (state) => state.authenticatedUser,

  loggedIn: (state) => (!!state.accessToken),

  authenticating: (state) => state.authenticating,
};

const actions = {
  async loginAction({ commit }, { username, password }) {
    commit(types.LOGIN);

    try {
      const response = await AuthService.login(username, password);
      commit(types.LOGIN_SUCCESS, response);

      // Redirect the user to the page he first tried to visit or to the home view
      await router.push(router.history.current.query.redirect || '/');

      return true;
    } catch (error) {
      commit(types.LOGIN_ERROR);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });

      return false;
    }
  },

  logoutAction({ commit }) {
    AuthService.logout();
    commit(types.LOGOUT_SUCCESS);
  },

  async registerAction({ commit }, user) {
    try {
      await AuthService.register(user);
      commit(types.REGISTER_SUCCESS, user);

      // Login after user is registered
      await actions.loginAction({ commit }, {
        username: user.username,
        password: user.password,
      });
    } catch (error) {
      commit(types.REGISTER_ERROR, error);

      Vue.notify({
        group: 'error',
        title: `${error.type} (${error.errorCode})`,
        text: error.message,
      });
    }
  },
};

const mutations = {
  [types.LOGIN](state) {
    state.authenticating = true;
  },

  [types.LOGIN_SUCCESS](state, data) {
    state.authenticatedUser = data.user;
    state.accessToken = data.token;
    state.authenticating = false;
  },

  [types.LOGIN_ERROR](state) {
    state.authenticatedUser = undefined;
    state.authenticating = false;
  },

  [types.REGISTER](state) {
    state.registerLoading = true;
    state.registerError = undefined;
  },

  [types.REGISTER_SUCCESS](state) {
    state.registerLoading = false;
    state.registerError = undefined;
  },

  [types.REGISTER_ERROR](state, error) {
    state.registerLoading = false;
    state.registerError = error;
  },

  [types.LOGOUT_SUCCESS](state) {
    state.authenticatedUser = undefined;
    state.accessToken = '';
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
