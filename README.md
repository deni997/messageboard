# MESSAGE BOARD #

Message board simplified Reddit-like application that shows:
- User can register and authenticate
- User can get list of cards
- User can create/update/delete cards (title, description, date)
- User can create/update/delete comment (text, date) on cards


## Setup

#### Database setup

1. Download and install MySQL database > [here](https://dev.mysql.com/downloads/installer/)
2. Import SQL structure from db/messageboard.sql
3. Update your username and password for your database

#### Server setup

1. > cd api
2. > npm install
3. > npm start:dev


-----

## Database Knowledge

## JavaScript Knowledge

* [Callbacks, Promises](https://www.youtube.com/watch?v=PoRJizFvM7s)
* [ES6 PRomises](https://www.youtube.com/watch?v=DHvZLI7Db8E)